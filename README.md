# Welcome to CS 490!

We'll be working mostly in GitLab. To get started: 

1. Read the Syllabus.MD file
2. Read the Schedule.MD file
3. Read and follow the directions in the "Discord Guidelines - CS 490" file

Our first meeting on 8/31 will be in the Classroom in the Pines. See PDF document for directions. Our second class will meet on Zoom. 

Looking forward to seeing you!
Heidi Ellis 


